
class Row {
  constructor(position) {
    this.rowIndex = position;
    this.rowCells = [];
    this.possibleValues = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  }

  removePossibleValueFromCellInRow(i, input, thisObject, row) {
    const rowObject = row;

    rowObject.possibleValues = rowObject.possibleValues.filter(val => val !== input[i]);
    console.log(`ROW INDEX : ${thisObject.cells[i].rowIndex}
    POSSIBLE VALUES - ROWS ${(thisObject.rows[thisObject.cells[i].rowIndex].possibleValues)}`);
    return this;
  }

  // removePossibleValueFromCellInRow2(i, input, thisObject, row) {
  //   const rowObject = row;
  //
  //   rowObject.possibleValues = rowObject.possibleValues.filter(val => val !== input[i]);
  //   console.log(`ROW INDEX : ${thisObject.cells[i].rowIndex}
  //   POSSIBLE VALUES - ROWS ${(thisObject.rows[thisObject.cells[i].rowIndex].possibleValues)}`);
  //   return this;
  // }

  removePossibleValueFromAssociatedCellsInRow(i, input, thisObject, row) {
    const rowCellsObject = row;

    for (let rc = 0; rc < 9; rc += 1) {
      rowCellsObject.rowCells[rc].possibleValues = rowCellsObject.rowCells[rc]
        .possibleValues.filter(value => (value !== input[i]));
      console.log(thisObject.rows[thisObject.cells[i].rowIndex].rowCells[rc]);
    }
    return this;
  }
}

module.exports = Row;
