
class Column {
  constructor(position) {
    this.columnIndex = position;
    this.columnCells = [];
    this.possibleValues = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  }

  removePossibleValueFromCellInColumn(i, input, thisObject, column) {
    const columnObject = column;
    columnObject.possibleValues = columnObject.possibleValues.filter(val => val !== input[i]);
    console.log(`COLUMN INDEX : ${thisObject.cells[i].columnIndex}
    POSSIBLE VALUES - COLUMNS ${(thisObject.columns[thisObject.cells[i].columnIndex].possibleValues)}`);
    return this;
  }

  removePossibleValueFromAssociatedCellsInColumn(i, input, thisObject, column) {
    const columnCellsObject = column;

    for (let rc = 0; rc < 9; rc += 1) {
      columnCellsObject.columnCells[rc].possibleValues = columnCellsObject.columnCells[rc]
        .possibleValues.filter(value => (value !== input[i]));
      console.log(thisObject.columns[thisObject.cells[i].columnIndex].columnCells[rc]);
    }
    return this;
  }
}

module.exports = Column;
